package de.paxii.clarinet.module.external;

import de.paxii.clarinet.insult.InsultThread;
import de.paxii.clarinet.module.Module;
import de.paxii.clarinet.module.ModuleCategory;

public class ModuleInsult extends Module {
  public ModuleInsult() {
    super("Insult", ModuleCategory.PLAYER);

    this.setVersion("1.1");
    this.setBuildVersion(19200);
    this.setCommand(true);
    this.setDescription("Sends an insult from evilinsult.com");
    this.setSyntax("insult");
  }

  @Override
  public void onEnable() {
    Thread insultThread = new InsultThread();
    insultThread.start();
    this.setEnabled(false);
  }

  @Override
  public void onCommand(String[] args) {
    this.setEnabled(true);
  }
}
