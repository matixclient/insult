package de.paxii.clarinet.insult;

import lombok.Value;

/**
 * evilinsult.com Insult
 */
@Value
public class Insult {

  private String insult;

}
