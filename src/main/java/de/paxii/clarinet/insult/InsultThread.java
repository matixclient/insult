package de.paxii.clarinet.insult;

import de.paxii.clarinet.util.chat.Chat;
import de.paxii.clarinet.util.web.JsonFetcher;

public class InsultThread extends Thread {

  private static final String INSULT_URL = "https://evilinsult.com/generate_insult.php?type=json&lang=en";

  @Override
  public void run() {
    int tries = 0;
    while (tries < 5) {
      Insult insult = JsonFetcher.get(INSULT_URL, Insult.class);

      if (insult != null) {
        Chat.sendChatMessage(insult.getInsult());

        return;
      }

      tries++;
    }

    Chat.printClientMessage("Could not get Insult after 5 retries.");
  }

}
